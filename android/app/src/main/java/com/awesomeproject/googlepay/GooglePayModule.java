package com.awesomeproject.googlepay;

import android.util.Log;
import androidx.annotation.NonNull;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentsClient;

import org.json.JSONObject;

//modif_1
public class GooglePayModule extends ReactContextBaseJavaModule {
    public GooglePayModule(ReactApplicationContext context) {
        super(context);
    }

    @NonNull
    @Override
    public String getName() {
        return "GooglePayModule";
    }

    //test synchrone
    @ReactMethod(isBlockingSynchronousMethod = true)
    public String callSynchronous(String token) {
        //Log.d("GooglePayModule", "Call api with token: " + token);
        return "GooglePayModule is working from native : " + token;
    }

    //test asynchrone
    @ReactMethod
    public void callApiEventOne(String name, String location, Promise promise) {
        try {
            Integer eventId = 4;
            promise.resolve(eventId);
        } catch(Exception e) {
            promise.reject("Create Event Error", e);
        }
    }

    //Verifie si device supporte GooglePay-----------------------------------
    @ReactMethod
    public void fetchCanUseGooglePay(Promise promise) {
        PaymentsClient paymentsClient = PaymentsUtil.createPaymentsClient(this.getReactApplicationContext());
        final JSONObject isReadyToPayJson = PaymentsUtil.getIsReadyToPayRequest();
        if (isReadyToPayJson == null) {
            return;
        }
        // The call to isReadyToPay is asynchronous and returns a Task. We need to provide an
        // OnCompleteListener to be triggered when the result of the call is known.
        IsReadyToPayRequest request = IsReadyToPayRequest.fromJson(isReadyToPayJson.toString());
        Task<Boolean> task = paymentsClient.isReadyToPay(request);
        task.addOnCompleteListener(
                completedTask -> {
                    if (completedTask.isSuccessful()) {
                        promise.resolve(completedTask.getResult());
                    } else {
                        Log.w("isReadyToPay failed", completedTask.getException());
                        promise.reject("isReadyToPay failed", completedTask.getException());
                    }
                });
    }
}